import { divide, get, isNil, map, round } from 'lodash';
import * as R from 'ramda';

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const getYearLast2Digits = (year) => {
  if (year === 2000) {
    return '00';
  }
  return year > 2000 ? year - 2000 : year - 1900;
}

const getQuarter = (datum) => {
  const month = datum.getMonth();
  return `Q${divide(month, 3) + 1}`;
}

const getMonth = (datum) => {
  const month = datum.getMonth();
  return months[month];
}

export const appendTimelineTooltip = ({ options, data, type, config }) => {
  const frequency = data.frequency
  if (type !== 'TimelineChart' || isNil(frequency)) {
    return options;
  }
  let timeFormatter = datum => datum.getFullYear();
  if (frequency === 'quarterly') {
    timeFormatter = datum => `${getQuarter(datum)} ${datum.getFullYear()}`;
  }
  if (frequency === 'monthly') {
    timeFormatter = datum => `${getMonth(datum)} ${getYearLast2Digits(datum.getFullYear())}`
  }
  const tooltipFonts = get(config, 'fonts.chart.tooltip', {});
  const primaryFontFamily = get(tooltipFonts, 'primary.fontFamily', 'bernino-sans-narrow-regular');
  const primaryFontSize = get(tooltipFonts, 'primary.fontSize', 12);
  const secondaryFontFamily = get(tooltipFonts, 'secondary.fontFamily', 'bernino-sans-bold');
  const secondaryFontSize = get(tooltipFonts, 'secondary.fontSize', 16);
  const secondaryFonts = `font-family: '${secondaryFontFamily}'; font-size: ${secondaryFontSize}px;`
  const primaryFonts = (color) => (`
    font-size: ${primaryFontSize}px;
    font-family: '${primaryFontFamily}';
    color: ${color};
    padding: 5px;
    border: 1px solid ${color};
    background: white;
    opacity: .8;
  `);

  return ({
    ...options,
    serie: {
      ...options.serie,
      tooltip: {
        layout: (serie, datum, color) => (`
          <div style="${primaryFonts(color)}">
            <div style="text-align: right;">
              ${map(get(datum, 'dimensionValues', {}), 'name').join('<br />')}
            </div>
            <div style="text-align: right;">
              ${timeFormatter(datum.x)}
            </div>
            <div style="text-align: right; ${secondaryFonts}">
              ${datum.formatedValue || round(datum.y, 2)}
            </div>
          </div>
        `)
      }
    }
  });
}

export const getShareData = ({
  chartData,
  chartOptions,
  config,
  type,
  headerProps,
  footerProps,
  dataflowId,
  units,
  properties,
  sdmxUrl,
  withCopyright
}) => ({
  config: {
    ...R.pick(['fonts', 'locale', 'sourceHeaders', 'dataflowId'], config),
    ...R.pick(['logo', 'owner', 'terms'], footerProps),
    dataflowId,
  },
  data: {
    ...headerProps,
    series: R.prop('series', chartData),
    source: R.propOr({}, 'source', footerProps),
    ...(type === 'TimelineChart' ? { frequency: R.prop('frequency', chartData) } : {}),
    copyright: withCopyright ? {} : null,
    share: {
      ...R.pick(['display', 'formaterIds'], config),
      focused: R.path(['share', 'focused'], chartData),
      chartDimension: R.path(['share', 'chartDimension'], chartData),
      source: sdmxUrl,
      units
    },
    informationsState: R.pipe(
      R.pick(['title', 'subtitle', 'source']),
      R.mapObjIndexed(R.prop('isDefault'))
    )(properties)
  },
  options: chartOptions,
});

export const refineShareData = (shareMode, type) => (shareData) => {
  const informationsState = R.path(['data', 'informationsState'], shareData);
  const latestRefine = R.pipe(
    R.when(
      R.always(type !== 'ChoroplethChart'),
      R.dissocPath(['data', 'series'])
    ),
    R.dissocPath(['data', 'uprs']),
    R.dissocPath(['data', 'frequency']),
    R.dissocPath(['data', 'range']),
    R.dissocPath(['data', 'informationsState']),
    R.when(
      R.always(informationsState.title),
      R.dissocPath(['data', 'title']),
    ),
    R.when(
      R.always(informationsState.subtitle),
      R.dissocPath(['data', 'subtitle']),
    ),
    R.when(
      R.always(informationsState.source),
      R.dissocPath(['data', 'source', 'label']),
    )
  );
  const snapshotRefine = R.pipe(
    R.dissocPath(['config', 'sourceHeaders']),
    R.dissocPath(['config', 'dataflowId']),
    R.dissocPath(['data', 'share', 'display']),
    R.dissocPath(['data', 'share', 'formaterIds']),
    R.dissocPath(['data', 'share', 'source']),
    R.dissocPath(['data', 'share', 'chartDimension']),
    R.dissocPath(['data', 'share', 'units']),
    R.dissocPath(['data', 'informationsState'])
  );
  return R.pipe(
    R.ifElse(
      () => shareMode === 'latest',
      latestRefine,
      snapshotRefine
    ),
    R.assocPath(['data', 'share', 'sharedate'], new Date().toISOString()),
  )(shareData)
};

