
export default {
  options: [
    { label: 'Default (fully responsive)', value: 'default' },
    { label: 'oecd.org/T4: 3 columns (width 270 px)', value: 'T4/3' },
    { label: 'oecd.org/T4: 4 columns (width 370 px)', value: 'T4/4' },
    { label: 'oecd.org/T4: 6 columns (width 560 px)', value: 'T4/6' },
    { label: 'oecd.org/T4: 8 columns (width 760 px)', value: 'T4/8' },
    { label: 'oecd.org/T4: 9 columns (width 855 px)', value: 'T4/9' },
    { label: 'oecd.org/T4: 12 columns (width 1140 px)', value: 'T4/12' }
  ],
  values: {
    default: { width: '100%', height: '100%' },
    'T4/3': { width: 270, height: 203 },
    'T4/4': { width: 370, height: 278 },
    'T4/6': { width: 560, height: 420 },
    'T4/8': { width: 760, height: 570 },
    'T4/9': { width: 855, height: 642 },
    'T4/12': { width: 1140, height: 855 }
  }
};
