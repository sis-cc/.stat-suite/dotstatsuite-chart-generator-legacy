import React, { Component } from 'react';
import { render } from 'react-dom';
import {
  Button, Position, Classes, Intent, EditableText, ProgressBar, Toaster, Tag, Tooltip, AnchorButton
} from '@blueprintjs/core';
import { map, isEmpty, size, pick, omit, get } from 'lodash';
import { parse as uParse, format as uFormat } from 'url';
import { parse as qParse } from 'qs';
import { Chart, RulesDriver, rules } from '@sis-cc/dotstatsuite-components';
import ConfigChart from './components/chart';
import '@blueprintjs/core/dist/blueprint.css';
import './assets/app.less';
import logo from './assets/images/OECD_logo.inline.svg';
import favicon from './assets/images/favicon.ico';
import configLabels from './labels';
import Select from 'react-select';
import { Helmet } from 'react-helmet';
import ReactGA from 'react-ga';
import { ThemeProvider } from '@material-ui/core/styles';
import { v7DataFormatPatch } from '@sis-cc/dotstatsuite-sdmxjs';
import mUiTheme from './theme';


import meta from '../package.json';
console.info(`[${process.env.NODE_ENV}#${process.env.TARGET_ENV}#${process.env.APP_ENV}]::[${meta.name}@${meta.version}]`);

const Toast = Toaster.create({ position: Position.TOP_CENTER });

class App extends Component {
  constructor(props) {
    super(props);

    this.charts = {
      BarChart: { icon: 'timeline-bar-chart', label: 'Bar' },
      RowChart: { icon: 'horizontal-bar-chart', label: 'Row' },
      ScatterChart: { icon: 'scatter-plot', label: 'Scatter' },
      HorizontalSymbolChart: { icon: 'gantt-chart', label: 'HSymbol' },
      VerticalSymbolChart: { icon: 'waterfall-chart', label: 'VSymbol' },
      TimelineChart: { icon: 'timeline-line-chart', label: 'Timeline' },
      StackedBarChart: { icon: 'stacked-chart', label: 'Stacked Bar' }
    };

    this.help = 'https://oecd.service-now.com/selfservice?id=kb_article&sysparm_article=KB0010301';

    this.config = { 
      read: get(props.config, 'chart.viewer.url'),
      write: get(props.config, 'chart.api.share.endpoint'),
      defaultSource: get(props.config, 'chart.viewer.source'),
      logo,
      owner: '©OECD',
      terms: { label: 'Terms & Conditions', link: 'https://www.oecd.org/termsandconditions/' },
      hasT4Formats: true
    };

    this.toast;
    this.apiEndpoint= get(props.config, 'default.api.endpoint');
    this.dataQuery = get(props.config, 'default.data.query');

    this.state = {
      type: 'BarChart',
      data: undefined,
      isFetching: false,
      uri: '',
      apiEndpoint: this.apiEndpoint,
      dataQuery: this.dataQuery,
      isDirty: false,
      locale: 'en',
      display: 'label',
      removeCopyright: false,
      removeLogo: false,
      name: null
    };

    this.locales = [
      { label: 'English', value: 'en' },
      { label: 'French', value: 'fr' }
    ];

    this.translations = {
      'chart.config.information.title.placeholder': 'Recommended number of character is around 60.',
      'chart.config.information.subtitle.placeholder': 'Recommended number of character is around 70.'
    };

    const gaTrackingId = get(props.config, 'GA.trackingID');
    if (gaTrackingId) {
      ReactGA.initialize(gaTrackingId);
      ReactGA.pageview(window.location.pathname);
      this.config.onShareGenerated = ({ responseJson, requestBody }) => {
        ReactGA.event({
          category: 'Share Generate',
          action: get(requestBody, 'type'),
          label: get(requestBody, 'data.data.title'),
          dimension1: get(responseJson, 'id'),
          dimension3: get(requestBody, 'share'),
        });
      };
      this.config.onMediaShare = ({ media, sharedData, shareResponse }) => {
        ReactGA.event({
          category: 'Share Media',
          action: get(sharedData, 'type'),
          label: get(sharedData, 'data.data.title'),
          dimension1: get(shareResponse, 'id'),
          dimension2: media,
          dimension3: get(sharedData, 'share'),
        });
      };
    }
    
    this.handleType = this.handleType.bind(this);
    this.handleFetch = this.handleFetch.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleApi = this.handleApi.bind(this);
    this.handleQuery = this.handleQuery.bind(this);
    this.changeLocale = this.changeLocale.bind(this);
    this.changeDisplay = this.changeDisplay.bind(this);
    this.changeRemoveCopyright = this.changeRemoveCopyright.bind(this);
    this.changeRemoveLogo = this.changeRemoveLogo.bind(this);
    this.getSourceLink = this.getSourceLink.bind(this);
  }

  componentWillMount() {
    this.handleFetch();
  }

  handleType = (type) => () => this.setState({ type });
  handleApi = (event) => this.setState({ apiEndpoint: event.target.value, isDirty: false });
  handleQuery = (event) => this.setState({ dataQuery: event.target.value, isDirty: false });
  handleReset = (event) => this.setState({ apiEndpoint: this.apiEndpoint, dataQuery: this.dataQuery, isDirty: false });
  handleUriEnter = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault(); // avoid onChange to be fired
      this.handleFetch();
    }
  };

  changeLocale = ({ value }) => this.setState({ locale: value }, this.handleFetch);

  changeDisplay = (display) => this.setState({ display });

  changeRemoveLogo = (value) => this.setState({ removeLogo: value });

  changeRemoveCopyright = (value) => this.setState({ removeCopyright: value });

  getSourceLink = () => {
    const query = this.state.dataQuery;
    const endpoint = this.state.apiEndpoint;
    if (!query || !endpoint) {
      return null;
    }
    const endpointMatch = endpoint.match(/(.*)\/SDMX-JSON\/data\//);
    const queryMatch = query.match(/([^\/]+)\//);
    if (!endpointMatch || !queryMatch) {
      return null;
    }
    return `${endpointMatch[1]}/Index.aspx?DataSetCode=${queryMatch[1]}`
  };
 
  uriFactory = (uri) => {
    const parsedUri = omit(uParse(uri), 'search');
    const parsedQs = qParse(parsedUri.query);
    const hackedQuery = { ...parsedQs, dimensionAtObservation: 'AllDimensions' };
    
    return uFormat({ ...parsedUri, query: hackedQuery })
  };

  headers = () => ({
    Accept: 'application/vnd.sdmx.data+json',
    'Accept-Language': this.state.locale,
  });

  handleFetch = () => {
    const uri = `${this.state.apiEndpoint}${this.state.dataQuery}`;

    if (this.state.uri === uri && this.state.locale === this.state.prevLocale) {
      Toast.dismiss(this.toast);
      this.toast = Toast.show({
        message: 'uri has not changed',
        intent: Intent.PRIMARY,
        iconName: 'info-sign',
      });
      return;
    }

    this.setState({ isFetching: true, isDirty: false });
    Toast.dismiss(this.toast);
    fetch(
      this.uriFactory(uri),
      { headers: this.headers() }
    ).then((response) => { // stop fetching, 'finally' is not native yet
        this.setState({ isFetching: false });
        return response;
      })
      .then((response) => { // server error
        if (response.ok) return response.json();
        throw new Error(`${response.status} ${response.statusText}`);
      })
      .then((json) => v7DataFormatPatch(json))
      .then((json) => { // handle sdmx errors
        console.log('json', json);
        const errors = rules.extractSdmxErrors(json);
        if (isEmpty(errors)) return json;
        throw new Error(errors.join('\n'));
      })
      .then((data) => { // handle sdmx data
        const { dimensions, name, observations } = rules.extractSdmxArtefacts(data);
        this.setState({ uri: uri, data, name, prevLocale: this.state.locale, isDirty: true });
        this.toast = Toast.show({
          message: `${size(observations)} observations and ${size(dimensions)} dimensions fetched`,
          intent: Intent.SUCCESS,
          iconName: 'endorsed',
        });
      })
      .catch((error) => {
        this.toast = Toast.show({
          message: error.message,
          intent: Intent.DANGER,
          iconName: 'error',
        });
      })
  };

  render = () => {
    return (
      <div className="app">
        <nav className="pt-navbar pt-fixed-top">
          <div className="pt-navbar-group pt-align-left">
            <div className="pt-navbar-heading">   
              <h5 className="app-title">
                <img src={logo} className="app-logo" />
                CHART GENERATOR
              </h5>
            </div>
          </div>
          <div className="pt-navbar-group pt-align-right">
            {
              map(this.charts, (chart, type) => (
                //<Tooltip content={chart.label} position={Position.BOTTOM} key={type}>
                  <Button
                    className={Classes.MINIMAL}
                    iconName={chart.icon}
                    onClick={this.handleType(type)}
                    active={type === this.state.type}
                    text={chart.label}
                    key={type}
                  />
                //</Tooltip>
              ))
            }
            <span className="pt-navbar-divider"></span>
            <Button
              className={Classes.MINIMAL}
              onClick={this.handleFetch}
              iconName="import"
              text="fetch"
            />
            <Button
              className={Classes.MINIMAL}
              onClick={this.handleReset}
              iconName="refresh"
              text="reset uri"
            />
            <span className="pt-navbar-divider"></span>
            <AnchorButton
              className={Classes.MINIMAL}
              iconName="help"
              target="_blank"
              intent={Intent.PRIMARY}
              href={this.help}
            />
          </div>
        </nav>
        <div className="app-pending">
          { this.state.isFetching ? <ProgressBar intent={Intent.PRIMARY} /> : null }
        </div>
        <div className="app-input">
          <label className="pt-label">
            <span>API Endpoint</span>
            <textarea
              className="pt-input pt-fill pt-monospace-text"
              dir="auto"
              value={this.state.apiEndpoint}
              onChange={this.handleApi}
              //onBlur={this.handleFetch}
              onKeyPress={this.handleUriEnter}
            >
            </textarea>
          </label>
          <label className="pt-label">
            <span>SDMX Query</span>
            <textarea
              className="pt-input pt-fill pt-monospace-text"
              dir="auto"
              value={this.state.dataQuery}
              onChange={this.handleQuery}
              //onBlur={this.handleFetch}
              onKeyPress={this.handleUriEnter}
            >
            </textarea>
          </label>
          <label className="pt-label">
            <span>Data Language</span>
            <Select
              style={{marginTop: 5}}
              clearable={false}
              onChange={this.changeLocale}
              options={this.locales}
              value={this.state.locale}
            />
          </label>
        </div>
        <div className="app-chart">
          <RulesDriver
            data={this.state.data}
            sdmxUrl={this.uriFactory(this.state.uri)}
            display={this.state.display}
            source={{ link: this.getSourceLink() }}
            type={this.state.type}
            render={({ chartData, chartOptions, headerProps, footerProps, properties, onChange, errors }) => (
              <ConfigChart
                config={{ ...this.config, sourceHeaders: this.headers(), display: this.state.display }}
                data={{
                  ...chartData,
                  ...headerProps,
                  ...footerProps,
                }}
                isFetching={this.state.isFetching}
                errors={errors}
                onChange={onChange}
                dataflowId={this.state.name}
                chartData={chartData}
                chartOptions={chartOptions}
                headerProps={headerProps}
                footerProps={footerProps}
                isDirty={this.state.isDirty}
                options={chartOptions}
                type={this.state.type}
                labels={configLabels}
                removeLogo={this.state.removeLogo}
                removeCopyright={this.state.removeCopyright}
                sdmxUrl={this.uriFactory(this.state.uri)}
                properties={{
                  ...properties,
                  display: {
                    id: 'display',
                    isActive: true,
                    onChange: this.changeDisplay,
                    options: [
                      { value: 'label' },
                      { value: 'code' },
                      { value: 'both' },
                    ],
                    value: this.state.display,
                  }
                }}
              />
            )}
          />
        </div>
      </div>
    );
  };
}

// Static config
(process.env.NODE_ENV === 'development'
  ? import(`./config/${process.env.APP_ENV}.json`)
  : fetch('config.json', { credentials: 'include' })
      .then((response) => {
        if (response.ok) return response.json();
        throw new Error(response.statusText);
      })
).then((json) => {
  setTimeout(
    () => {
      const config = Object.assign({}, get(json, 'base'), get(json, process.env.TARGET_ENV));
      render(
        <ThemeProvider theme={mUiTheme}>
          <Helmet title="Chart Generator">
            <link rel="icon" type="image/x-icon" href={favicon} />
          </Helmet>
          <App config={config} />
        </ThemeProvider>,
        document.getElementById('root'),
      );
    },
    0
  )
}).catch((error) => {
  console.log('failed to load static config');
  render(
    <ThemeProvider theme={mUiTheme}>
      <Helmet title="Chart Generator">
        <link rel="icon" type="image/x-icon" href={favicon} />
      </Helmet>
      <App />
    </ThemeProvider>,
    document.getElementById('root'),
  );
});

